import React, { Component } from 'react';
import { TextInput, Dimensions } from 'react-native';

class InputText extends Component {
  constructor(props) {
    super(props);
    this.state = { text: 'Useless Placeholder' };
  }

  render() {
    return (
      <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1, width: Dimensions.get('window').width }}
        onChangeText={(text) => this.setState({text})}
        value={this.state.text}
      />
    );
  }
}

//var width = Dimensions.get('window').width; //full width

export default InputText;

// App registration and rendering
//AppRegistry.registerComponent('AwesomeProject', () => UselessTextInput);