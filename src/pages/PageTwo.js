import React, { Component } from 'react';
import { View, Text, AsyncStorage, TouchableOpacity, Image, Button, StyleSheet, ScrollView, PixelRatio } from 'react-native';
import { Actions } from 'react-native-router-flux';

const Sound = require('react-native-sound');

export default class PageOne extends Component {
    constructor () {
        super();

        this.onPressButton = this.onPressButton.bind(this);
        this.countdown = this.countdown.bind(this);
        this.tick = this.tick.bind(this);
        this.countdownStop = this.countdownStop.bind(this);
        this.startCountdown = this.startCountdown.bind(this);
        this.renderSound = this.renderSound.bind(this);
        this.renderRolesMarker = this.renderRolesMarker.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
        this.whoSpeaks = this.whoSpeaks.bind(this);
        this.setRole = this.setRole.bind(this);


        Sound.setCategory('Ambient', true); // true = mixWithOthers

            this.playSoundBundle = () => {
              const s = new Sound('march.mp3', Sound.MAIN_BUNDLE, (e) => {
                if (e) {
                  console.log('error', e);
                } else {
                  s.setSpeed(1);
                  console.log('duration', s.getDuration());
                  s.play(() => s.release()); // Release when it's done so we're not using up resources
                }
              });
            };



            this.playSoundLooped = () => {
              if (this.state.loopingSound) {
                return;
              }
              const s = new Sound('march.mp3', Sound.MAIN_BUNDLE, (e) => {
                if (e) {
                  console.log('error', e);
                }
                s.setNumberOfLoops(-1);
                s.play();
              });
              this.setState({loopingSound: s});
            };

            this.stopSoundLooped = () => {
              if (!this.state.loopingSound) {
                return;
              }

              this.state.loopingSound
                .stop()
                .release();
              this.setState({loopingSound: null});
            };

            this.playSoundFromRequire = () => {
              const s = new Sound(requireAudio, (e) => {
                if (e) {
                  console.log('error', e);
                  return;
                }

                s.play(() => s.release());
              });
            };

            this.state = {
              loopingSound: undefined,
            };

            //Second sound

            var whoosh = new Sound('notification.mp3', Sound.MAIN_BUNDLE, (error) => {
              if (error) {
                console.log('failed to load the sound', error);
                return;
              }
              // loaded successfully
              console.log('duration in seconds: ' + whoosh.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());
            });

            this.playNotifification = () => {
                whoosh.play((success) => {
                  if (success) {
                    console.log('successfully finished playing');
                  } else {
                    console.log('playback failed due to audio decoding errors');
                  }
                });
            }
    }

    componentWillMount () {
        this.setState({
            сountdownMin: '0',
            сountdownSec: '00',
            active: [],
            killed: [],
            muted: [],
            cured: [],
            accused: [],
            quitted: [],
            players: '4',
            town: '180000',
            townSetTime:'',
            citizenSetTime:'',
            citizen: '30000',
            townSetTime:'',
            intervalID: '',
            whoSpeaks:'city',
            role_Comissar: true,
            role_Doctor: true,
            role_Don: true,
            role_Maniac: true,
            role_Putana: true,
            role_Set_Mafia: [],
            role: []
        });

         this.getAsyncStorage();

         console.log(' game+++    state ', this.state);
    }

    getAsyncStorage = (item) => {

        AsyncStorage.getItem('mafia_store').then((storedItem)=> {
             console.log('AsyncStorage.getItem(item)... ', storedItem);
             console.log('AsyncStorage.getItem(item)... ', storedItem);

             this.setState(JSON.parse(storedItem));

             if(this.state.townSetTime === '') {
                this.setState({
                    townSetTime: this.state.town
                });
             }

             if(this.state.citizenSetTime === '') {
                this.setState({
                    citizenSetTime: this.state.citizen
                });
             }

             console.log("___Game state:____ ", this.state);
        });
     }

    setAsyncStorage = (items) => {

        AsyncStorage.setItem('mafia_store', JSON.stringify(items));
         AsyncStorage.getItem('mafia_store').then((storedItem)=> {
            console.log('AsyncStorage.getItem(item)... ', storedItem);
            return storedItem;
         });
    }

    whoSpeaks (who) {

        this.setState({
            whoSpeaks: who
        });

        this.countdownStop();
    }

    countdown () {

        let countdownInterval = window.setInterval(this.tick, 1000);

        this.setState({
            intervalID: countdownInterval
        });
    }

    countdownStop () {

        this.setState({
            сountdownMin: '0',
            сountdownSec: '00',
            town: 0,
            citizen: 0,
        });

        clearInterval(this.state.intervalID);
    }

    startCountdown () {
        this.getAsyncStorage();

            clearInterval(this.state.intervalID);

            this.setState({
                cityCountdownMin: '',
                cityCountdownSec: '',
                town: this.state.townSetTime,
                citizen: this.state.citizenSetTime
            });

            this.countdown();
    }

    tick () {
        var time;

        if (this.state.whoSpeaks === 'city') {
            time  = this.state.town;
        }

        if (this.state.whoSpeaks === 'citizen') {
            time  = this.state.citizen;
        }

       // time = time * (60 * 1000);
        let timeLeft = time - 1000;

        let minutes = Math.floor((time % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((time % (1000 * 60)) / 1000);

        if(time === 0 || minutes === 0 && seconds === 0) {
            this.playNotifification();
            this.countdownStop();
        }

        this.setState({
           сountdown: (timeLeft / 1000) / 60,
           сountdownMin: minutes,
           сountdownSec: seconds
        });


         if (this.state.whoSpeaks === 'city') {
             this.setState({
                town: timeLeft
              });
         }

         if (this.state.whoSpeaks === 'citizen') {
            this.setState({
                citizen: timeLeft
            });
         }
    }

    onPressButton = (e) => {
         console.log(this,' -- ', e)
    }

    changeStatus (index, status) {

         console.log("______index_______", index ," ______status_______", status);

        let activeArr = this.state.active;
        let killedArr = this.state.killed;

        let mutedArr = this.state.muted;
        let curedArr = this.state.cured;
        let accusedArr = this.state.accused;
        let quittedArr = this.state.quitted;

        if(status === 'Active') {
            activeArr = [];
            activeArr[index] !== status ?  activeArr[index] = status : activeArr[index] = '';
        }

        if(status === 'Killed') {
            killedArr[index] = status;

            console.log("this.state.killed[index] ...", this.state.killed[index]);
        }

        if(status === 'Muted') {
            mutedArr = [];
            mutedArr[index] !== status ?  mutedArr[index] = status : mutedArr[index] = '';
        }

        if(status === 'Cured') {
            curedArr = [];
            curedArr[index] !== status ?  curedArr[index] = status : curedArr[index] = '';
        }

        if(status === 'Accused') {

            accusedArr[index] !== status ?  accusedArr[index] = status : accusedArr[index] = '';
        }

        if(status === 'Quitted') {
            quittedArr[index] = status;
        }

        let newActiveArr = activeArr;
        let newKilledArr = killedArr;
        let newMutedArr = mutedArr;
        let newCuredArr = curedArr;
        let newAccusedArr = accusedArr;
        let newQuittedArr = quittedArr;

        this.setState({
            killed: newKilledArr,
            muted: newMutedArr,
            cured: newCuredArr,
            accused: newAccusedArr,
            quitted: newQuittedArr,
            active: activeArr
        });
    }

    setRole (index ) {
    const role_Don  = this.state.role_Don;
    const role_Mafia  = this.state.role_Mafia;
    const role_Putana  = this.state.role_Putana;
    const role_Maniac  = this.state.role_Maniac;
    const role_Comissar  = this.state.role_Comissar;
    const role_Doctor  = this.state.role_Doctor;

    let arr = this.state.role;

    console.log("...... ", this.state.role[index]);

        if (!this.state.role[index]) {

            if (this.state.role_Don === true) {
                 arr[index] = 'Don';
                this.setState({
                    role: arr
                });

                return;

                 console.log("...... ", this.state.role);
            } else {

              arr[index] = 'Mafia';

              this.setState({
                 role: arr
              });

               console.log("...... ", this.state.role);
               return;
            }
        }

        if (this.state.role[index] == 'Don') {
               arr[index] = 'Mafia';

               this.setState({
                   role: arr
               });

                console.log("...... ", this.state.role);
                return;
        }

        if (this.state.role[index] == 'Mafia') {

            if (this.state.role_Putana === true) {
                arr[index] = 'Putana';

                this.setState({
                    role: arr
                });

                 console.log("...... ", this.state.role);
                 return;
            } else {

                if (this.state.role[index] == 'Maniac') {

                    if (this.state.role_Comissar === true) {
                        arr[index] = 'Comissar';

                        this.setState({
                            role: arr
                        });
                        return;
                    } else {

                        if (this.state.role_Doctor === true) {
                            arr[index] = 'Doctor';

                            this.setState({
                                 role: arr
                            });
                        } else {
                            arr[index] = '';

                            this.setState({
                                role: arr
                            });

                            return;
                        }
                    }
                }
            }
        }

        if (this.state.role[index] == 'Putana') {

            if (this.state.role_Maniac === true) {
                arr[index] = 'Maniac';
                this.setState({
                    role: arr
                });
            } else {

                if (this.state.role_Comissar === true) {
                    arr[index] = 'Comissar';
                    this.setState({
                        role: arr
                    });
                } else {
                    if (this.state.role_Doctor === true) {
                        arr[index] = 'Doctor';
                        this.setState({
                            role: arr
                        });
                    } else {
                         arr[index] = '';
                         this.setState({
                             role: arr
                         });
                     }
                }
            }
        }

        if (this.state.role[index] == 'Maniac') {

            if (this.state.role_Comissar === true) {
                arr[index] = 'Comissar';
                this.setState({
                    role: arr
                });
            } else {
                if (this.state.role_Doctor === true) {
                    arr[index] = 'Doctor';
                    this.setState({
                        role: arr
                    });
                } else {
                    arr[index] = '';
                    this.setState({
                        role: arr
                    });
                }
            }
        }

        if (this.state.role[index] == 'Comissar') {

            if (this.state.role_Doctor === true) {
                arr[index] = 'Doctor';
                this.setState({
                    role: arr
                });
            } else {
                arr[index] = '';
                this.setState({
                    role: arr
                });
            }
        }

        if (this.state.role[index] == 'Doctor') {
                arr[index] = '';
                this.setState({
                    role: arr
                });
        }
    }

    renderTouchable() {

        var rows = [];

        for (let index = 0; index < this.state.players; index++) {
            console.log("___state.players______ ", this.state.players );

             rows.push(
                <View key={'row-' + index} style={this.state.quitted[index] === 'Quitted'? styles.executedRow : styles.regularRow}>

                    <TouchableOpacity
                        style={this.state.active[index] === 'Active'? styles.active : styles.noactive}
                        onPress={()=>{this.changeStatus(index, 'Active')}}
                     >
                         <Text style={styles.num}>{index + 1}</Text>
                    </TouchableOpacity>

                    { this.state.role[index] === 'Don' &&

                        <TouchableOpacity
                            style={styles.imgDon}
                            onPress={()=>{this.setRole(index)}}
                         >
                          <Image
                             style={{width: 50, height: 50, alignItems:'center', marginBottom: 10, flexDirection:'row', borderRadius: 100}}
                             key={index}
                             source={require('./../img/mafia-ico.png')}
                           />
                        </TouchableOpacity>
                    }

                    { !this.state.role[index] &&

                        <TouchableOpacity
                            style={styles.imgNone}
                            onPress={()=>{this.setRole(index)}}
                         >
                          <Image
                             style={{width: 50, height: 50, alignItems:'center', marginBottom: 10, flexDirection:'row', borderRadius: 100}}
                             key={index}
                             source={require('./../img/mafia-ico.png')}
                           />
                        </TouchableOpacity>
                    }

                    { this.state.role[index] === 'Mafia' &&

                        <TouchableOpacity
                            style={styles.imgMafia}
                            onPress={()=>{this.setRole(index)}}
                         >
                          <Image
                             style={{width: 50, height: 50, alignItems:'center', marginBottom: 10, flexDirection:'row', borderRadius: 100}}
                             key={index}
                             source={require('./../img/mafia-ico.png')}
                           />
                        </TouchableOpacity>
                    }

                    { this.state.role[index] === 'Putana' &&

                        <TouchableOpacity
                            style={styles.imgPutana}
                            onPress={()=>{this.setRole(index)}}
                         >
                          <Image
                             style={{width: 50, height: 50, alignItems:'center', marginBottom: 10, flexDirection:'row', borderRadius: 100}}
                             key={index}
                             source={require('./../img/mafia-ico.png')}
                           />
                        </TouchableOpacity>

                    }

                    { this.state.role[index] === 'Maniac' &&

                        <TouchableOpacity
                            style={styles.imgManiac}
                            onPress={()=>{this.setRole(index)}}
                         >
                          <Image
                             style={{width: 50, height: 50, alignItems:'center', marginBottom: 10, flexDirection:'row', borderRadius: 100}}
                             key={index}
                             source={require('./../img/mafia-ico.png')}
                           />
                        </TouchableOpacity>
                    }

                    { this.state.role[index] === 'Comissar' &&

                        <TouchableOpacity
                            style={styles.imgComissar}
                            onPress={()=>{this.setRole(index)}}
                         >
                          <Image
                             style={{width: 50, height: 50, alignItems:'center', marginBottom: 10, flexDirection:'row', borderRadius: 100}}
                             key={index}
                             source={require('./../img/mafia-ico.png')}
                           />
                        </TouchableOpacity>

                    }

                    { this.state.role[index] === 'Doctor' &&

                        <TouchableOpacity
                            style={styles.imgDoctor}
                            onPress={()=>{this.setRole(index)}}
                         >
                          <Image
                             style={{width: 50, height: 50, alignItems:'center', marginBottom: 10, flexDirection:'row', borderRadius: 100}}
                             key={index}
                             source={require('./../img/mafia-ico.png')}
                           />
                        </TouchableOpacity>
                    }

                     <TouchableOpacity
                        style={this.state.killed[index] === 'Killed'? styles.killed : styles.regularButton}
                        onPress={()=>{this.changeStatus(index, 'Killed')}}
                     >
                      <Text style={styles.buttonText}>Killed</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={this.state.muted[index] === 'Muted'? styles.muted : styles.regularButton}
                        onPress={()=>{this.changeStatus(index, 'Muted')}}
                     >
                      <Text style={styles.buttonText}>Muted</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={this.state.cured[index] === 'Cured'? styles.cured : styles.regularButton}
                        onPress={()=>{this.changeStatus(index, 'Cured')}}
                     >
                      <Text style={styles.buttonText}>Cured</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={this.state.accused[index] === 'Accused'? styles.accused : styles.regularButton}
                        onPress={()=>{this.changeStatus(index, 'Accused')}}
                     >
                      <Text style={styles.buttonText}>Accused</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={this.state.quitted[index] === 'Quitted'? styles.quitted : styles.regularButton}
                        onPress={()=>{this.changeStatus(index, 'Quitted')}}
                     >
                      <Text style={styles.buttonText}> Executed </Text>
                    </TouchableOpacity>

                 </View>
              );
         }
        return(
            <View>
                {rows}
            </View>
        );
    }

    renderSound () {

    return <Image 
            style={{flexDirection:'column', justifyContent:'space-around', width: null, height: 150, borderRadius: 150, marginBottom: 10, flex: 1}}
            source={require('./../img/aqv.png')} 
          >
            <Text style={styles.countdownHeader}>Ночь (вкл. музыку):</Text>

                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start'}}>
                    <TouchableOpacity
                            style={{margin: 10}}
                            onPress={this.playSoundLooped}
                         >
                            <Image 
                              style={{width: 50, height: 50, borderRadius: 150, marginBottom: 20}}
                              source={require('./../img/play.png')} 
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style= {{margin: 10}}
                            onPress={this.stopSoundLooped}
                         >
                         <Image 
                           style={{width: 50, height: 50, borderRadius: 150, marginBottom: 20}}
                           source={require('./../img/stop.png')} 
                         />
                     </TouchableOpacity>
                </View>
           </Image>
   }

   renderRolesMarker () {
            const role_Don  = this.state.role_Don;
            const role_Mafia  = this.state.role_Mafia;
            const role_Putana  = this.state.role_Putana;
            const role_Maniac  = this.state.role_Maniac;
            const role_Comissar  = this.state.role_Comissar;
            const role_Doctor  = this.state.role_Doctor;

        return (
             <View style={{margin: 20}}>
                <Text style={{fontSize: 30}}>Активные роли:</Text>
                <Text>(Кликните на иконке игрока, чтобы установить маркер роли)</Text>

                 <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-around'}}>
                    {role_Don === true &&
                        <Text style={styles.rolesDon}>Дон мафии</Text>
                    }

                    <Text style={styles.rolesMafia}>Мафия {this.state.mafia}</Text>

                    {role_Putana === true &&
                        <Text style={styles.rolesPutana}>Путана</Text>
                    }
                    {role_Maniac === true &&
                        <Text style={styles.rolesManiac}>Маньяк</Text>
                    }
                    {role_Comissar === true &&
                        <Text style={styles.rolesKomissar}>Комиссар</Text>
                    }
                    {role_Doctor === true &&
                       <Text style={styles.rolesDoctor}>Доктор</Text>
                    }
                </View>
            </View>
        );
   }

  render() {
    return (
        <View style={{marginTop: 50}}>

             <ScrollView>
                    <View style={{flexDirection:'row', justifyContent:'space-around', marginBottom: 0, flex: 1}}>
                        <TouchableOpacity
                            style={this.state.whoSpeaks === 'city'? styles.selectedButtonOutline : styles.regularButtonOutline}
                            onPress={()=>{this.whoSpeaks('city')}}
                         >
                          <Text style={styles.buttonTextOutline}>Город обсуждает:</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={this.state.whoSpeaks === 'citizen'? styles.selectedButtonOutline : styles.regularButtonOutline}
                            onPress={()=>{this.whoSpeaks('citizen')}}
                         >
                          <Text style={styles.buttonTextOutline}>Горожанин говорит:</Text>
                        </TouchableOpacity>

                      </View>

                    <View style={{flex: 1, flexDirection:'row', justifyContent: 'space-around'}}>

                            <Image 
                                style={{width: 190, height: 180, borderRadius: 150, marginBottom: 0}}
                                source={require('./../img/timer.png')} 
                              >
                                    <Text style={styles.countdownNum}>{this.state.сountdownMin}:{this.state.сountdownSec}</Text>
                                    <View style={{flex:1, flexDirection:'row', justifyContent: 'center'}}>
                                        <TouchableOpacity
                                           style= {{marginRight: 10}}
                                           onPress={this.startCountdown}
                                         >
                                            <Image 
                                              style={{width: 50, height: 50, borderRadius: 150}}
                                              source={require('./../img/play.png')} 
                                            />
                                         </TouchableOpacity>

                                         <TouchableOpacity
                                             style= {{marginLeft: 10}}
                                             onPress={this.countdownStop}
                                            >
                                            <Image 
                                              style={{width: 50, height: 50, borderRadius: 150}}
                                              source={require('./../img/stop.png')} 
                                            />
                                        </TouchableOpacity>
                                    </View>
                            </Image>
                        </View>

                    {this.renderSound()}

                    {this.renderTouchable()}

                   {this.renderRolesMarker()}

             </ScrollView>
      </View>
    )
  }
}

const onButtonPress = () => {
  console.log('Button has been pressed!');
};

const styles = StyleSheet.create({
    countdownHeader: {
        fontSize: 15,
        textAlign: 'center',
        margin: 10,
        color: '#ffbe00'
      },

      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#de0d0d'
      },

      countdownNum: {
        fontSize: 60,
        textAlign: 'center',
        marginTop: 50,
        color: '#D50000'
      },

      imgcontainer: {
        backgroundColor:'transparent',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        flex: 1,
        // remove width and height to override fixed static size
        width: null,
        height: null
       },

    num: {
       color: '#ffbe00',
       fontSize: 20,
       margin: 10
    },

    active: {
      width: 50,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 3 / PixelRatio.get(),
      borderColor: '#fff800',
      backgroundColor: '#000',
      borderRadius: 10
    },

    noactive: {
      width: 50,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 3 / PixelRatio.get(),
      borderColor: '#fff800',
      backgroundColor: '#fff',
      borderRadius: 10
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    buttonstyle: {
        width: 150,
        margin: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#bc9047',
        borderRadius: 2
    },

    buttonText: {
        color: '#ffffff',
        fontSize: 20,
        margin: 10
    },

    regularButton: {
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       borderWidth: 3 / PixelRatio.get(),
       borderColor: '#fff800',
       backgroundColor: '#000',
       borderRadius: 10,
       paddingTop: 10,
       paddingBottom: 10
     },

     regularButtonOutline: {
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       borderWidth: 10 / PixelRatio.get(),
       borderColor: '#c7c7c7',
       backgroundColor: '#fff',
       borderRadius: 10,
       margin: 10
     },

     selectedButtonOutline: {
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       borderWidth: 10 / PixelRatio.get(),
       borderColor: '#fff800',
       backgroundColor: '#fff',
       borderRadius: 10,
       margin: 10
     },

      header: {
           borderBottomWidth: 1,
           borderBottomColor: 'black',
      },

  feature: {
    padding: 20,
    alignSelf: 'stretch',
  },

  killed: {
    borderWidth: 2,
    borderColor: '#000000',
    backgroundColor: '#fff',
    padding: 5,
    height: 50
  },

  muted: {
    borderWidth: 2,
    borderColor: '#ffbe00',
    backgroundColor: '#fff',
    padding: 5,
    height: 50
  },

  cured: {
    borderWidth: 2,
    borderColor: '#ffbe00',
    backgroundColor: '#fff',
    padding: 5,
    height: 50
  },

  accused: {
    borderWidth: 2,
    borderColor: '#ffbe00',
    backgroundColor: '#fff',
    padding: 5,
    height: 50
  },

  quitted: {
    borderWidth: 1,
    borderColor: '#333333',
    backgroundColor: '#fff',
    padding: 5,
    height: 50
  },

  buttonText: {
      color: '#ffbe00',
      fontSize: 10,
      margin: 10
  },

  buttonTextOutline: {
      color: '#666666',
      fontSize: 15,
      margin: 10
  },

  regularRow: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems:'center',
    marginBottom: 10,
    flexDirection:'row'
  },

  executedRow: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems:'center',
    marginBottom: 10,
    flexDirection:'row',
    opacity: 0.3
  },

  rolesDon: {
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       borderWidth: 10 / PixelRatio.get(),
       borderColor: '#750000',
       backgroundColor: '#fff',
       borderRadius: 10,
       margin: 10,
       paddingLeft: 5

  },

  rolesMafia: {
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       borderWidth: 10 / PixelRatio.get(),
       borderColor: '#ec0202',
       backgroundColor: '#fff',
       borderRadius: 10,
       margin: 10,
       paddingLeft: 5
  },

  rolesPutana: {
       flexDirection: 'column',
       justifyContent: 'center',
       alignItems: 'center',
       borderWidth: 10 / PixelRatio.get(),
       borderColor: '#a300f1',
       backgroundColor: '#fff',
       borderRadius: 10,
       margin: 10,
       paddingLeft: 5
  },

  rolesManiac: {
         flexDirection: 'column',
         justifyContent: 'center',
         alignItems: 'center',
         borderWidth: 10 / PixelRatio.get(),
         borderColor: '#f17e00',
         backgroundColor: '#fff',
         borderRadius: 10,
         margin: 10,
         paddingLeft: 5
  },

  rolesKomissar: {
         flexDirection: 'column',
         justifyContent: 'center',
         alignItems: 'center',
         borderWidth: 10 / PixelRatio.get(),
         borderColor: '#0051f1',
         backgroundColor: '#fff',
         borderRadius: 10,
         margin: 10,
         paddingLeft: 5
  },

  rolesDoctor: {
         flexDirection: 'column',
         justifyContent: 'center',
         alignItems: 'center',
         borderWidth: 10 / PixelRatio.get(),
         borderColor: '#08c55f',
         backgroundColor: '#fff',
         borderRadius: 10,
         margin: 10,
         paddingLeft: 5
  },

  imgNone: {
    width: 50,
    height: 50,
    borderRadius: 30
  },

  imgDon: {
    width: 55,
    height: 55,
    borderRadius: 30,
    borderWidth: 10 / PixelRatio.get(),
    borderColor: '#750000'
  },

    imgMafia: {
        width: 55,
        height: 55,
        borderRadius: 30,
        borderWidth: 10 / PixelRatio.get(),
        borderColor: '#ec0202'
      },

      imgPutana: {
        width: 55,
        height: 55,
        borderRadius: 30,
        borderWidth: 10 / PixelRatio.get(),
        borderColor: '#a300f1'
      },

      imgManiac: {
        width: 55,
        height: 55,
        borderRadius: 30,
        borderWidth: 10 / PixelRatio.get(),
        borderColor: '#ffcc00'
      },

      imgComissar: {
        width: 55,
        height: 55,
        borderRadius: 30,
        borderWidth: 10 / PixelRatio.get(),
        borderColor: '#0051f1'
      },

      imgDoctor: {
        width: 55,
        height: 55,
        borderRadius: 30,
        borderWidth: 10 / PixelRatio.get(),
        borderColor: '#08c55f'
      }
});