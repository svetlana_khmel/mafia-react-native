import React, { Component } from 'react';
import { View, Text, Button, Image, StyleSheet, TouchableOpacity, TouchableNativeFeedback, TouchableHighlight, PixelRatio } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class PageOne extends Component {
  render() {

  const goToPageTwo = () => Actions.pageTwo({text: 'Hello World!'});

    return (

     <Image
       source={require('./../img/bg.png')}
       style={styles.container}
     >

        <Image 
          style={{width: 155, height: 155, borderRadius: 150, marginBottom: 20}}
          source={require('./../img/mafia-logo.png')} 
        />

        <TouchableOpacity
            style={styles.buttonstyleOne}
            onPress={Actions.pageOne}
            activeOpacity={0.9}
         >
          <Text style={styles.buttonText}>Игроки</Text>
        </TouchableOpacity>

        <TouchableOpacity
            style={styles.buttonstyleTwo}
            onPress={Actions.pageTwo}
            activeOpacity={0.9}
         >
          <Text style={styles.buttonText}>Игра</Text>
        </TouchableOpacity>

        <TouchableOpacity
            style={styles.buttonstyleFour}
            onPress={Actions.pageThree}
            activeOpacity={0.9}
         >
          <Text style={styles.buttonText}>Настройки</Text>
        </TouchableOpacity>

        <TouchableOpacity
            style={styles.buttonstyleFour}
            onPress={Actions.pageFour}
            activeOpacity={0.9}
         >
          <Text style={styles.buttonText}>Правила</Text>
        </TouchableOpacity>
      </Image>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: undefined,
    height: undefined,
    backgroundColor:'transparent',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonstyleOne: {
    width: 150,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 3 / PixelRatio.get(),
    borderColor: '#fff800',
    backgroundColor: '#000',
    borderRadius: 10
  },

  buttonstyleTwo: {
    width: 150,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 3 / PixelRatio.get(),
    borderColor: '#fff800',
    backgroundColor: '#000',
    borderRadius: 10
  },

  buttonstyleThree: {
    width: 150,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 3 / PixelRatio.get(),
    borderColor: '#fff800',
    backgroundColor: '#000',
    borderRadius: 10
  },

  buttonstyleFour: {
    width: 150,
    margin: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 3 / PixelRatio.get(),
    borderColor: '#fff800',
    backgroundColor: '#000',
    borderRadius: 10
  },

   buttonText: {
    color: '#ffbe00',
    fontSize: 20,
    margin: 10
  }

});

