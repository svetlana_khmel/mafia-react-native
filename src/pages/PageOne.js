import React, { Component } from 'react';
import { View, Text, StyleSheet, PixelRatio, Picker, Dimensions, Button, ScrollView, AsyncStorage, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class PageOne extends Component {

    constructor (props) {
        super (props);

        this.state = {
            players: this.getAsyncStorage() || '4',
            mafia: this.getAsyncStorage() || '1',
            autoCalculation : 'true'
        }

        this.onValueChanged = this.onValueChanged.bind(this);
        this.onCalculateMafia = this.onCalculateMafia.bind(this);
        this.getAsyncStorage = this.getAsyncStorage.bind(this);
        this.setAsyncStorage = this.setAsyncStorage.bind(this);
    }

    componentWillMount () {
        this.getAsyncStorage();

        console.log("1)______start state ", this.state);
    }

    onValueChanged = (players) => {
      this.setState({players: players});
      console.log('   onValueChanged   ', players);

      this.setAsyncStorage({players: players})
      this.onCalculateMafia(players);
    };

    onValueMafiaChanged = (mafia) => {
        this.setState({
            mafia: mafia
        });

        this.setAsyncStorage({mafia: mafia})
    }

    getAsyncStorage = (item) => {

        AsyncStorage.getItem('mafia_store').then((storedItem)=> {
             console.log('AsyncStorage.getItem(item)... ', storedItem);
             console.log('AsyncStorage.getItem(item)... ', storedItem);

             this.setState(JSON.parse(storedItem));

             console.log('AsyncStorage.getItem(item)... ', this.state.players);
             console.log("______start state ", this.state);

              this.setAsyncStorage({players: this.state.players})
        });
    }

    setAsyncStorage = (items) => {

        AsyncStorage.setItem('mafia_store', JSON.stringify(items));

         AsyncStorage.getItem('mafia_store').then((storedItem) => {
                     console.log('AsyncStorage.getItem(item)... ', storedItem);

                    return storedItem;
         });
    }

    onCalculateMafia = (playersNum) => {
      let players = parseInt(playersNum);
      let mafia = 0;

      if (players === 4) {
        mafia = 1;
      }
      if (players === 5) {
        mafia = 1;
      }
      if (players === 6) {
        mafia = 1;
      }
      if (players === 7) {
        mafia = 2;
      }
      if (players === 8) {
        mafia = 2;
      }
      if (players === 9) {
        mafia = 3;
      }
      if (players === 10) {
        mafia = 3;
      }
      if (players === 11) {
        mafia = 3;
      }
      if (players === 12) {
        mafia = 4;
      }
      if (players === 13) {
        mafia = 4;
      }
      if (players === 14) {
        mafia = 4;
      }
      if (players === 15) {
        mafia = 5;
      }
      if (players === 16) {
        mafia = 5;
      }
      if (players === 17) {
        mafia = 5;
      }
      if (players === 18) {
        mafia = 6;
      }
      if (players === 19) {
        mafia = 6;
      }
      if (players === 20) {
        mafia = 6;
      }

      console.log('   onCalculateMafia   ', mafia, '  ', players);

      this.setState({mafia: mafia});
      this.setAsyncStorage({mafia: mafia, players: players})
    };

    renderMafia () {

        console.log('   this.state.autoCalculation   ', this.state.autoCalculation, '  this.state.mafia ', this.state.mafia);

        if (this.state.autoCalculation === "true") {
            return(
                <Text style={styles.instructions}>{this.state.mafia}</Text>
            );
        }

        if (this.state.autoCalculation === false) {
            return(
                <Picker
                     style={{ flex: 1, height: 50, width: Dimensions.get('window').width, color: '#333333'}}
                     selectedValue={this.state.mafia}
                     onValueChange={this.onValueMafiaChanged}
                   >
                   <Picker.Item label="1" value="1" />
                   <Picker.Item label="2" value="2" />
                   <Picker.Item label="3" value="3" />
                   <Picker.Item label="4" value="4" />
                   <Picker.Item label="5" value="5" />
                   <Picker.Item label="6" value="6" />
                </Picker>
            );
        }
    }

  render() {

  const goToPageTwo = () => Actions.pageTwo({text: 'Hello World!'});

      return (
        <View style={{margin: 10}}>
                <Text>{this.props.text}</Text>

                <ScrollView>
                    <View style={styles.rowPickerStyle}>
                        <Text style={styles.welcome}>Город говорит:</Text>

                        <Picker
                              style={{ flex: 1, height: 50, width: Dimensions.get('window').width, color: '#333333'}}
                              selectedValue={this.state.players}
                              onValueChange={this.onValueChanged}
                            >
                            <Picker.Item label="4 игрока" value="4" />
                            <Picker.Item label="5 игроков" value="5" />
                            <Picker.Item label="6 игроков" value="6" />
                            <Picker.Item label="7 игроков" value="7" />
                            <Picker.Item label="8 игроков" value="8" />
                            <Picker.Item label="9 игроков" value="9" />
                            <Picker.Item label="10 игроков" value="10" />
                            <Picker.Item label="11 игроков" value="11" />
                            <Picker.Item label="12 игроков" value="12" />
                            <Picker.Item label="13 игроков" value="13" />
                            <Picker.Item label="14 игроков" value="14" />
                            <Picker.Item label="15 игроков" value="15" />
                            <Picker.Item label="16 игроков" value="16" />
                            <Picker.Item label="17 игроков" value="17" />
                            <Picker.Item label="18 игроков" value="18" />
                            <Picker.Item label="19 игроков" value="19" />
                            <Picker.Item label="20 игроков" value="20" />
                         </Picker>
                    </View>
                    <View style={styles.rowPickerStyle}>
                       <Text style={styles.welcome}>Мафии в игре:</Text>
                        {this.renderMafia()}
                    </View>

                    <View style={{flex: 1, flexDirection: 'column',  alignItems: 'center', margin: 30}}>

                        <TouchableOpacity
                             style={styles.buttonstyleTwo}
                             onPress={Actions.pageTwo}
                             activeOpacity={0.9}
                        >
                               <Text style={styles.buttonText}>Игра</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
              </View>
        )
  }
}

const styles = StyleSheet.create({
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#ffbe00'
    },

    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'space-between'
    },

    switchStyle: {
        margin: 10
    },

    rowStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#dadada',
        borderBottomWidth: 1 / PixelRatio.get(),
        paddingBottom: 5
    },

     rowPickerStyle: {
         borderBottomColor: '#dadada',
         borderBottomWidth: 1 / PixelRatio.get(),
         paddingBottom: 5
     },

      buttonstyleTwo: {
          width: 150,
          margin: 10,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 3 / PixelRatio.get(),
          borderColor: '#fff800',
          backgroundColor: '#000',
          borderRadius: 10
      },

      buttonText: {
          color: '#ffbe00',
          fontSize: 20,
          margin: 10
      }
});

