import React, { Component } from 'react';
import { View, Text, ScrollView, Switch, StyleSheet, borderBottomWidth, PixelRatio, Picker, Dimensions, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

export default class PageThree extends Component {

   constructor() {
        super();

        this.state = {
          role_Don: this.getAsyncStorage() || true,
          role_Comissar: this.getAsyncStorage() || true,
          role_Maniac: this.getAsyncStorage() || true,
          role_Putana: this.getAsyncStorage() || true,
          role_Doctor: this.getAsyncStorage() || true,
          autoCalculation: this.getAsyncStorage() || true,
          town: 60000,
          citizen: 30000
        }

        this.onValueChanged = this.onValueChanged.bind(this);
        this.onValueTownChanged = this.onValueTownChanged.bind(this);
        this.onValueCitizenChanged = this.onValueCitizenChanged.bind(this);
   }

   componentWillMount () {
         this.getAsyncStorage();
   }

   onValueChanged = (role, value) => {
        var Obj = '{'+ '"' + role + '"' + ':'+ value +'}';
        var state = JSON.parse(Obj);

        this.setState(state);
        this.setAsyncStorage(state);
   }

    onValueTownChanged = (town) => {
       this.setState({
           town: town
       });

       this.setAsyncStorage({town: town})
   }

   onValueCitizenChanged = (citizen) => {
       this.setState({
           citizen: citizen
       });

       this.setAsyncStorage({citizen: citizen});
   }

   onPickerValueChanged = (timer, value) => {

    console.log('   onValueChanged   ', timer, '   ..', value);
        var Obj = '{'+ '"' + timer + '"' + ':'+ value +'}';
        var state = JSON.parse(Obj);
        this.setState(state);

        this.setAsyncStorage(state);
   }

   getAsyncStorage = (item) => {

       AsyncStorage.getItem('mafia_store').then((storedItem)=> {
            console.log('AsyncStorage.getItem(item)... ', storedItem);
            this.setState(JSON.parse(storedItem));

            console.log('AsyncStorage.getItem(item)... ', this.state.players);
       });
   }

   setAsyncStorage = (items) => {

         AsyncStorage.getItem('mafia_store').then((storedItem)=> {
             let Obj = _.merge({}, JSON.parse(storedItem), items);
             AsyncStorage.setItem('mafia_store', JSON.stringify(Obj));


             console.log('.....THIS STATE...... ', this.state);
        });

        AsyncStorage.getItem('mafia_store').then((storedItem)=> {
             console.log('AsyncStorage.getItem(item)... ', storedItem);
             return storedItem;
        });
   }

  render() {
    return (
      <View style={{margin: 10, flex: 1}}>

        <Text>{this.props.text}</Text>

        <ScrollView>

            <Text style={styles.welcome}>Роли:</Text>

            <View style={styles.rowStyle}>
                <Text style={styles.welcome}>Дон мафии</Text>

                <Switch
                 onValueChange={(value) => {this.onValueChanged('role_Don', value)}}
                 style={styles.switchStyle}
                 value={this.state.role_Don} />
            </View>

            <View style={styles.rowStyle}>
                <Text style={styles.welcome}>Комиссар</Text>
                <Switch
                 onValueChange={(value) => {this.onValueChanged('role_Comissar', value)}}
                 style={styles.switchStyle}
                 value={this.state.role_Comissar} />
            </View>

            <View style={styles.rowStyle}>
                <Text style={styles.welcome}>Маньяк</Text>
                <Switch
                 onValueChange={(value) => {this.onValueChanged('role_Maniac', value)}}
                 style={styles.switchStyle}
                 value={this.state.role_Maniac} />
            </View>

            <View style={styles.rowStyle}>
                <Text style={styles.welcome}>Путана</Text>
                <Switch
                 onValueChange={(value) => {this.onValueChanged('role_Putana', value)}}
                 style={styles.switchStyle}
                 value={this.state.role_Putana} />
            </View>

            <View style={styles.rowStyle}>
                <Text style={styles.welcome}>Доктор</Text>
                <Switch
                 onValueChange={(value) => {this.onValueChanged('role_Doctor', value)}}
                 style={styles.switchStyle}
                 value={this.state.role_Doctor} />
            </View>

            <View style={styles.rowPickerStyle}>
                <Text style={styles.welcome}>Город говорит:</Text>

                <Picker
                      style={{ flex: 1, height: 50, width: Dimensions.get('window').width, color: '#333333'}}
                      selectedValue={this.state.town}
                      onValueChange={this.onValueTownChanged}
                    >
                    <Picker.Item label="1 мин" value="60000" />
                    <Picker.Item label="2 мин" value="120000" />
                    <Picker.Item label="3 мин" value="180000" />
                    <Picker.Item label="4 мин" value="240000" />
                    <Picker.Item label="5 мин" value="300000" />
                 </Picker>
            </View>

            <View style={styles.rowPickerStyle}>
                <Text style={styles.welcome}>Выступление:</Text>

                <Picker
                      style={{ flex: 1, height: 50, width: Dimensions.get('window').width, color: '#333333'}}
                      selectedValue={this.state.citizen}
                      onValueChange={this.onValueCitizenChanged}
                    >
                    <Picker.Item label="30 сек" value="30000" />
                    <Picker.Item label="1 мин" value="60000" />
                </Picker>

            </View>

            <View style={styles.rowStyle}>
                <Text style={styles.welcome}>Авто расчет мафии:</Text>
                <Switch
                 onValueChange={(value) => {this.onValueChanged('autoCalculation', value)}}
                 style={styles.switchStyle}
                 value={this.state.autoCalculation} />
            </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#de0d0d'
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'space-between'
      },
      switchStyle: {
        margin: 10
      },
      rowStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#dadada',
        borderBottomWidth: 1 / PixelRatio.get(),
        paddingBottom: 5
      },
      rowPickerStyle: {
        borderBottomColor: '#dadada',
        borderBottomWidth: 1 / PixelRatio.get(),
        paddingBottom: 5
      }
});


