# README #

### What is this repository for? ###

* "Mafia host" application helps to play social game "Mafia". Available in russian language only.

[![en_badge_web_generic.png](https://bitbucket.org/repo/9pKMbE6/images/3446325083-en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=com.mafia)

React-native-sound, AsyncStorage , React-native-router-flux, JS (React Native), Html, Webpack, Nodejs/Express


Screenshots:

![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1491854850/1_urlmdr.png)
![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1491430139/set_ywztua.png)
![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1491854898/3_yknjxo.png)
![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1491430143/rules_limisw.png)

---



 

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

* Configuration

Brew install node
Brew install watchman
Npm install -g react-native-cil
react-native init mafia

react-native run-ios
react-native run-android

IS BETTER TO START WITH:
 react-native start

https://developer.android.com/studio/index.html?utm_source=android-studio

Install environment:
https://facebook.github.io/react-native/docs/getting-started.html



Release:
http://facebook.github.io/react-native/docs/signed-apk-android.html

Generating the release APK
Simply run the following in a terminal:

$ cd android && ./gradlew assembleRelease

Generate key
 keytool -genkey -v -keystore mafia.keystore -alias mafia_main -keyalg RSA -keysize 2048 -validity 10000


* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## Install Java ##

Don't rely on Oracle to install Java properly on your Mac.

Use Homebrew:

brew update
brew cask install java
If you want to manage multiple versions of Java on your Mac, consider using jenv.


java -version


Еo run a different version of Java, either specify the full path, or use the java_home tool:

% /usr/libexec/java_home -v 1.8.0_06 --exec javac -version

For example, to uninstall 8u6:

% rm -rf jdk1.8.0_06.jdk