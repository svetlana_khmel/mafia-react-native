// NAVIGATION


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  ListView,
  WebView,
  Switch,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  Alert,
  Platform,
  Picker
} from 'react-native';

import { Router, Scene } from 'react-native-router-flux';
import Launch from './src/pages/launch';
import PageOne from './src/pages/PageOne';
import PageTwo from './src/pages/PageTwo';
import PageThree from './src/pages/PageThree';
import PageFour from './src/pages/PageFour';



import Header from './src/components/header'; 
import InputText from './src/components/inputText';

export default class mafia extends Component {

constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']),
      language: "java",
      trueSwitchIsOn: true,
      falseSwitchIsOn: false
    };
  }

  render() {
      return (
        <Router>
          <Scene key="root">
            <Scene key="launch" component={Launch} title="Launch" initial={true} />
            <Scene key="pageOne" component={PageOne} title="Players"/>
            <Scene key="pageTwo" component={PageTwo} title="Game" />
            <Scene key="pageThree" component={PageThree} title="Settings" />
            <Scene key="pageFour" component={PageFour} title="Rules" />
          </Scene>
        </Router>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('mafia', () => mafia);
